;;;; CSci 1901 Project - Spring 2012
;;;; PhageWars++ Player AI

;;;======================;;;
;;;  SUBMISSION DETAILS  ;;;
;;;======================;;;

;; List both partners' information below.
;; Leave the second list blank if you worked alone.
(define authors 
  '((
     "Ramith Jayatilleka"   ;; Author 1 Name
     "jaya0089"   ;; Author 1 X500
     "4531667"   ;; Author 1 ID
     "2"   ;; Author 1 Section
     )
    (
     ""   ;; Author 2 Name
     ""   ;; Author 2 X500
     ""   ;; Author 2 ID
     ""   ;; Author 2 Section
     )))

;; CSELabs Machine Tested On: 
;;

;;============;;
;;  AI Rules  ;;
;;===============================================================;;
;; v8
;; If any attacks incoming within 2 turns, defend self.
;; Else attack nearest opposing cell.
;;===============================================================;;

;;;====================;;;
;;;  Player-Procedure  ;;;
;;;====================;;;

(define player-procedure
  (let () 

    ;;===================;;
    ;; Helper Procedures ;;
    ;;===============================================================;;
    ;; Include procedures used by get-move that are not available in ;;
    ;;  the util.scm or help.scm file.                               ;;
    ;; Note: PhageWars++.scm loads util.scm and help.scm , so you do ;;
    ;;  not need to load it from this file.                          ;;
    ;; You also have access to the constants defined inside of       ;;
    ;;  PhageWars++.scm - Constants are wrapped in asterisks.        ;;
    ;;===============================================================;;
	
    ;; Returns a random-element from a list.
    (define (random-element lst)
		(list-ref lst (random (length lst))))
    
	;; Makes a random move
    (define (make-random-move player board)
		(let ((my-cells (get-cell-list player board)))
			(list (make-move (car (random-element my-cells)) 
				(car (random-element board))))))

	;; Displays board
	(define (display-board board)
		(display "-----  BOARD:  -----\n")
		(for-each (lambda (x) (display x)(newline)) board)
		(newline))
	
	;; Displays queue
	(define (display-queue queue)
		(display "-----  QUEUE:  -----\n")
		(for-each (lambda (x) (display x)(newline)) (cdr queue))
		(newline))
	
	;; Mergesort function that works on a list of key-value pairs (sorts by numeric key, least to greatest).
	(define (mergesort lst)
		
		;; Merge function (iterative)
		(define (merge left right return)
			(cond
				((null? left) (append (reverse return) right)) ;; one end case
				((null? right) (append (reverse return) left)) ;; other end case
				((<= (caar left) (caar right)) (merge (cdr left) right (cons (car left) return))) ;; if the first left element is the lowest, add that to the return
				(else (merge left (cdr right) (cons (car right) return))) ;; else add the first right element to the return
			)
		)
		
		;; Split function (count is how many elements should go into left list). Left list is built in reverse (iterative)
		;; call left with (), right with the original list, count is how many elements should be in left
		(define (split left right count)
			(if (zero? count) ;; done
				(cons (reverse left) right) ;; return a pair of the split lists.
				(split (cons (car right) left) (cdr right) (- count 1)) ;; whittle down right and add to left (in reverse)
			)
		)
		
		;; Lists of length 1 are sorted
		(if (<= (length lst) 1)
			lst
			(let*
				(
					(unsorted-lr-pair (split () lst (floor (/ (length lst) 2))));; Split apart the list into a pair of two unsorted sublists
					(left (mergesort (car unsorted-lr-pair)));; sort left sublist
					(right (mergesort (cdr unsorted-lr-pair)));; sort right sublist
				)
				(merge left right ()) ;; merge both sorted sublists
			)
		)
	)
	
	;; Gets turns between two cells
	(define (get-time a b board)
		(ceiling (/ (distance a b board) *MOVE-SPEED*)))
	
	
	;; >>>>> THIS IS OBSOLETE <<<<<
	;; Selects the closest cell to start in the cell-list.
	;; Start is a cell number, cell-list is a list of cell numbers.
	;; CELL-LIST DOES NOT TAKE IN AN EMPTY LIST
	; (define (get-closest-old start cell-list board)
		; (if (null? cell-list)
			; (error "get-closest was passed an empty-list") ; 0 element list
			; (let helper
				; (
					; (lst (cdr cell-list))
					; (record (car cell-list))
				; )
				; (cond
					; ((null? lst) record) ; done
					; ((iscloser? start (car lst) record board) (helper (cdr lst) (car lst))) ; current record broken
					; (else (helper (cdr lst) record)) ; current record stands
				; )
			; )
		; )
	; )
	
	;; >>>>> THIS IS OBSOLETE <<<<<
	;; Attacks the nearest enemy cell
	;; start is a cell number
	;; Returns the proper move
	; (define (attack-nearest-enemy player start board)
		; (make-move
			; start
			; (get-closest-old
				; start
				; (append
					; (get-cell-number-list 'n board)
					; (get-cell-number-list (get-enemy player) board)
				; )
				; board
			; )
		; )
	; )
	
	;; Takes in an id and an expression.
	;; If the expression is evaluated properly,
	;; Displays the id, the value of the expression, and a newline, then returns the value of the expression.
	;; Extremely useful for debugging, since it can help locate an error, since scheme doesn't give you any help on its own.
	(define (try id value)
		(display id) (display " WORKS: ")
		(display value)
		(newline)
		value)
		
	;; Safe take, same as (take lst n) in util.scm, but if not enough elements, returns as many elements as it could take
	(define (safe-take lst n)
		(if (or (null? lst) (zero? n))
			()
			(cons (car lst) (safe-take (cdr lst) (- n 1)))))
	
	;; Gets n cell-ids close to start
	;; Uses by-distance
	(define (get-close-cells start n by-distance)
		(map car (safe-take (vector-ref by-distance start) n))) ;; map car because by-distance contains (cell-id . time-to-cell) pairs
	
	;; Gets the cell in cell-list that is closest to start
	;; cell-list is a list of cell ids you want to check for closeness
	(define (get-closest start cell-list by-distance)
		(if (null? cell-list) ;; check if cell-list actually has some ids
			(error "get-closest was passed an empty-list") ; 0 element list
			(let helper ((lst (vector-ref by-distance start))) ;; loops through all the cells in order of their closeness from start
				(cond
					((null? lst) (error "List passed to get-closest did not contain any cells on the board")) ;; bad input in cell list
					((element? (caar lst) cell-list) (caar lst)) ;; if this cell is in cell-list, return its id
					(else (helper (cdr lst)));; else keep searching
				)
			)
		)
	)
	
	;; Tests if a queued move is an attack by enemy on player
	(define (incoming-attack? queued-move player enemy board)
		(and
			(eq? enemy (get-owner-queued queued-move)) ;; check if move is owned by enemy
			(eq? player (get-owner (get-destination-queued queued-move) board)))) ;; check if move is targeting one of player's cells
	
	
	
    ;;====================;;
    ;; Get-Move Procedure ;;
    ;;===============================================================;;
    ;; This is the procedure called by dots++.scm to get your move.  ;;
    ;; Returns a line-position object.
    ;;===============================================================;;

	;; Main procedure
    (define get-move ;(get-move player queue board)
	
		;; Environment that the get-move procedure carries around with it
		;; contains static data that is compiled once and can then be referenced
		(let*
			(
				(precompiled #f) ;; boolean indicating whether compilation has been done
				(by-distance #f) ;; vector of lists of (cell-id . time-to) pairs, containing how close in distance and time every cell is to every other
			)
			
			;; Actual get-move procedure. Carries surrounding let with it.
			(lambda (player queue board)
			
				;; This let calculates useful dynamic data that changes every turn (or in other words, depends on the input to get-move).
				(let*
					(
						(enemy (other-player player)) ;; enemy player symbol
						(my-cell-ids (get-cell-number-list player board)) ;; list of ids of my cells
						(enemy-cell-ids (get-cell-number-list enemy board)) ;; list of ids of my enemy's cells
						(neutral-cell-ids (get-cell-number-list 'n board)) ;; list of ids of neutral cells
						(opposing-cell-ids (append neutral-cell-ids enemy-cell-ids)) ;; list of ids of cells not belonging to me
						(incoming-attacks (filter (lambda (x) (incoming-attack? x player enemy board)) (cdr queue))) ;; list of queued-moves that are attacking me
						(my-targeted-cells (map (lambda (x) (cons (get-destination-queued x) (get-time-queued x))) incoming-attacks)) ;; list of (cell-id . time-left) pairs that contain all incoming attacks.
					)
					
					;; Useful displays
					(display+ "\nPLAYER: " player "\nENEMY: " enemy "\n\n")
					(display-board board)
					(display-queue queue)
					
					;; Do precompiling on first turn
					(if (not precompiled)
						(begin 
							(set! precompiled #t) ;; Make sure that you don't do this every turn
							
							;; Indice zero is empty, the data for each cell is stored at the indice equal to their cell-id
							;; At each indice of this vector, there is a list of (cell-id . time-to-indice-cell) pairs
							;; these pairs are sorted by how close the cell represented by cell-id is to the cell represented by the indice
							(set! by-distance (make-vector (1+ (length board))))
							(let loop-board ((lst (map car board))) ;; loop through every cell-id
								(if (not (null? lst)) ;; done
									(begin
										(vector-set!
											by-distance
											(car lst) ;; This indice represents the current cell
											(map ;; this turns the list of (distance . cell-id) pairs into a list of (cell . time-to-cell) pairs
												(lambda (x) (cons (cdr x) (ceiling (/ (car x) *MOVE-SPEED*))))
												(mergesort ;; sort all cells into list closest to (car lst). including (car lst).
													(map ;; turn list of cell-ids into list of (distance . cell-id) pairs. These are (key, value) pairs for mergesort
														(lambda (x) (cons (distance (car lst) x board) x))
														(map car board) ;; get all cell ids
													)
												)
											)
										)
										(loop-board (cdr lst)) ;; continue this for every cell
									)
								)
							)
						)
					)
									
					
					;; This let builds a list of the moves
					(let loop-my-cells
						(
							(lst my-cell-ids)
							(return '())
						)
						
						(if (null? lst) ;; done case
							return ;; return the completed list of moves
							(cond ;;  <<<<< THIS IS THE DECISION MAKING CONDITIONAL! IT IS EVALUATED FOR EVERY PLAYER-OWNED CELL, EVERY TURN! <<<<<
								((any? ;; tests if the cell making its move is going to be attacked within 2 turns
									(lambda (x) (<= (cdr x) 2)) ;; test if the incoming attack will land in 2 or less turns
									(filter ;; leave only attacks on the cell choosing its move
										(lambda (x) (equal? (car x) (car lst))) ;; test if the incoming attack refers to the cell choosing its move
										my-targeted-cells))
									(loop-my-cells (cdr lst) return) ;; if so, this cell will defend (don't add onto the list of moves)
								)
								(else ;; if not defending,
									(loop-my-cells ;; then attack the nearest enemy or neutral cell
										(cdr lst)
										(cons ;; add onto the list of moves
											(make-move
												(car lst) ;; current cell
												(get-closest (car lst) opposing-cell-ids by-distance) ;; gets the closest enemy cell to the current cell
											)
											return
										)
									)
								)
							)
						)
					)
				)
			)
		)
	)

    ;; Return get-move procedure
    get-move

    )) ;; End of player-procedure
    
