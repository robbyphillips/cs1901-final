;;;; CSci 1901 Project - Spring 2012
;;;; PhageWars++ Player AI

;;;======================;;;
;;;  SUBMISSION DETAILS  ;;;
;;;======================;;;

;; List both partners' information below.
;; Leave the second list blank if you worked alone.
(define authors 
  '((
     "Rob Phillips"   ;; Author 1 Name
     "phil0531"   ;; Author 1 X500
     "3706637"   ;; Author 1 ID
     "2"   ;; Author 1 Section
     )
    (
     ""   ;; Author 2 Name
     ""   ;; Author 2 X500
     ""   ;; Author 2 ID
     ""   ;; Author 2 Section
     )))

;; CSELabs Machine Tested On: 
;;


;;;====================;;;
;;;  Player-Procedure  ;;;
;;;====================;;;
(define player-procedure
  (let () 

    ;;===================;;
    ;; Helper Procedures ;;
    ;;===============================================================;;
    ;; Include procedures used by get-move that are not available in ;;
    ;;  the util.scm or help.scm file.                               ;;
    ;; Note: PhageWars++.scm loads util.scm and help.scm , so you do ;;
    ;;  not need to load it from this file.                          ;;
    ;; You also have access to the constants defined inside of       ;;
    ;;  PhageWars++.scm - Constants are wrapped in asterisks.        ;;
    ;;===============================================================;;

    ;; REMOVE-FRONT
    ;; input: queue object
    ;; return: queue without the first item
    ;; modified from help.scm to return a value
    (define (remove-front queue)
      (if (null? (cdr queue)) 
        (error "Queue is already empty, cannot remove front")
        (begin
          (set-cdr! queue (cddr queue))
          queue)))

    ;; GET-CLOSEST-CELL
    ;; Returns the closest cell in lst
    (define (get-closest-cell c lst board)
      (newline)
      (display+ "get-closest-cell args:" c "; " lst) (newline)
      (define (helper l closest-yet)
        (cond ((null? l)
               (display+ "closest-cell: " closest-yet)(newline)
               closest-yet)
              ((null? closest-yet) (helper l (car l)))
              ((iscloser? c closest-yet (car l) board)
               (helper (cdr l) closest-yet))
              (else
                (helper (cdr l) (car l)))))
      (helper lst ()))

    ;; UNDER-ATTACK?
    ;; return #t if cell is under attack
    (define (under-attack? cell player queue)
      (define (helper q)
        (cond ((null? (cdr q)) #f)
              ((and
                 (equal? (other-player player)
                         (get-owner-queued (check-front q)))
                 (= cell (get-destination-queued (check-front q))))
               #t)
              (else
                (helper (cons 'queue (cddr q))))))
      (helper queue))

    (define (filter-queue proc queue)
      (cons 'queue (filter proc (cdr queue))))

    ;; GET-INCOMING-MOVES
    ;; returns a queue object containing only moves with a destination of cell within turns
    (define (get-incoming-moves cell turns queue)
      (filter-queue
        (lambda (x) 
          (and (= cell (get-destination-queued x))
               (>= turns (get-time-queued x))))
        queue))

    ;; GET-INCOMING-ATKS
    ;; returns a queue object containing only hostile moves with destination of cell
    (define (get-incoming-atks cell turns board queue)
      (filter-queue
        (lambda (x)
          (and (= cell (get-destination-queued x))
               (>= turns (get-time-queued x))
               (eq? (other-player (get-owner cell board)) (get-owner-queued x))))
        queue))

    ;; GET-INCOMING-SUPPORT
    ;; returns a queue object containing only friendly moves with destination of cell
    (define (get-incoming-support cell turns board queue)
      (filter-queue
        (lambda (x)
          (and (= cell (get-destination-queued x))
               (>= turns (get-time-queued x))
               (eq? (get-owner cell board) (get-owner-queued x))))
        queue))

    ;; WILL-SURVIVE?
    ;; returns #t if the cell will not change ownership next turn
    (define (will-survive? cell player board queue)
      (let ((income (get-cell-income cell board))
            (unit-count (get-cell-units cell board))
            (q (get-incoming-moves cell 1 queue)))
        (define (helper)
          (cond ((null? (cdr q)) (>= unit-count 0))
                ((equal? player (get-owner-queued (check-front q)))
                 (set! unit-count (+ unit-count (unit-count-queued (check-front q))))
                 (remove-front q)
                 (helper))
                (else
                  (set! unit-count (- unit-count (unit-count-queued (check-front q))))
                  (remove-front q)
                  (helper))))
        (helper)))

    ;; GET-NEXT-LOSSES
    ;; return a list of cell numbers that will be captured from player next turn
    (define (get-next-losses player board queue)
      (define (helper cells losses)
        (cond ((null? cells) losses) 
              ((will-survive? (caar cells) player board queue)
               (helper (cdr cells) losses))
              (else
                (helper (cdr cells) (cons (caar cells) losses)))))
      (helper (get-cell-list player board) ()))

    ;; GET-SURVIVORS
    ;; return a list of cell numbers that will not be captured by currently incoming atks
    (define (get-survivors player board queue)
      (define (helper cells survivors)
        (cond ((null? cells) survivors)
              ((will-survive? (caar cells) player board queue)
               (helper (cdr cells) (cons (caar cells) survivors)))
              (else
                (helper (cdr cells) survivors))))
      (helper (get-cell-list player board) ()))


    ;; GET-TARGETED-CELLS
    ;; return a list of cells that are under attack
    (define (get-targeted-cells player board)
      (define (helper l val)
        (cond ((null? l) val)
              ((under-attack? (caar l))
               (helper (cdr l) (cons (car l) val)))
              (else
                (helper (cdr l) val))))
      (helper (get-cell-list player) ()))

    ;; GET-CELL-INCOME
    ;; inputs: cell number and board
    ;; return: cell income per turn, integer
    (define (get-cell-income cell-number board)
      (list-ref (get-cell cell-number board) 3))

    ;; GET-CELL-UNITS
    (define (get-cell-units cell-number board)
      (list-ref (get-cell cell-number board) 4))

    ;; CAPTURE?
    ;; return #t if c1 can capture c2
    (define (capture? c1 c2 board #!optional bonus)
      (let ((time (ceiling (/ (distance c1 c2 board) *MOVE-SPEED*)))
            (attack-strength (get-attack-count c1 board))
            ;(c2-def (get-remaining-count c2 board))
            (c2-def (get-cell-units c2 board))
            ;(c2-growth (get-cell-income c2 board))
            (enrage (get-enrage-value *CURRENT-TURN*))
            (bonus (if (eq? bonus #!default) 0 bonus)))
        ;(>= attack-strength (+ c2-current-str (* c2-growth time)))))
        (>= (+ (* attack-strength enrage) bonus) c2-def)))

    ;; CELL-NUMS
    ;; inputs: a list of cells
    ;; output: a list of cell numbers
    (define (cell-nums ls)
      (if (null? ls) ()
        (cons (caar ls) (cell-nums (cdr ls)))))

    (define (stronger-cell? c1 c2 board)
      (> (get-cell-units c1 board) (get-cell-units c2 board)))

    (define (make-sorted-cell-list test cell lst board)
      (cond ((null? lst) (cons cell ()))
            ((test cell (car lst) board)
             (cons cell lst))
            (else
              (cons (car lst) (make-sorted-cell-list test cell (cdr lst) board)))))

    (define (sort-cells test lst board)
      (cond ((null? lst) ())
            (else
              (make-sorted-cell-list test (car lst) (sort-cells test (cdr lst) board) board))))

    ;; MULTI-MOVE
    ;; inputs: a list of cell numbers and destination cell number
    ;; output: a list of move objects from each cell number in list to destination
    (define (multi-move cell-num-list destination)
      (if (null? cell-num-list) ()
        (cons (make-move (car cell-num-list) destination)
              (multi-move (cdr cell-num-list) destination))))

    (define (land-grab player cell-num-lst board)
      (define (helper cell-num-lst moves-lst)
        (display+ "moves-lst: " moves-lst)(newline)
        (if (null? cell-num-lst) moves-lst
          (let ((n (get-closest-cell (car cell-num-lst) (cell-nums (get-cell-list *NEUTRAL-SYMBOL* board)) board))
                (e (get-closest-cell (car cell-num-lst) (cell-nums (get-cell-list (other-player player) board)) board)))
            (display+ "n: " n)(newline)(display+ "e: " e)(newline)
            (cond ((null? n)
                   (helper (cdr cell-num-lst) (cons (make-move (car cell-num-lst) e) moves-lst)))
                  ((null? e)
                   (helper (cdr cell-num-lst) (cons (make-move (car cell-num-lst) n) moves-lst)))
                  (else 
                    (helper (cdr cell-num-lst) (cons (make-move (car cell-num-lst) (car cell-num-lst)) moves-lst)))))))
      (helper cell-num-lst ()))

    ;; I be scheming and making moves
    (define (plan-moves player board q)
      (let ((team ()) 
            (team-str 0) 
            (moves ()) 
            (enrage (get-enrage-value *CURRENT-TURN*))
            (my-cells
              (sort-cells stronger-cell? (cell-nums (get-cell-list player board)) board))
            (enemy-cells
              (sort-cells stronger-cell? 
                          (append (get-survivors *NEUTRAL-SYMBOL* board q)
                                  (get-survivors (other-player player) board q))
                          board)))
        (define (plan-helper friendlies enemies)
          (newline)
          (display "mine sorted by str: ")(display my-cells)(newline)
          (display "friendlies: ")(display friendlies)(newline)
          (display "team: ")(display team)(newline)
          (display "team-str: ")(display team-str)(newline)
          ;(display "PLAYER: ")(display player) (newline)
          ;(display "my survivors: ")(display (get-survivors player board q))(newline)
          ;(display "n survivors:  ")(display (get-survivors *neutral-symbol* board q))(newline)
          ;(display "e survivors:  ")(display (get-survivors (other-player player) board q))(newline)
          (display "enemies sorted by str: ") (display enemy-cells)(newline)
          (display "enemies: ")(display enemies)(newline)
          (display "moves: ")(display moves) (newline)(newline)

          (cond ((and (null? friendlies) (null? moves))
                 (land-grab player team board))
                ((null? friendlies) moves)
                ((null? enemies)
                 (set! team (cons (car friendlies) team))
                 (set! team-str (+ team-str (get-attack-count (car friendlies) board)))
                 (plan-helper (cdr friendlies) enemy-cells))
                ((capture? (car friendlies) (car enemies) board (* (get-enrage-value *CURRENT-TURN*) team-str))
                 (set! moves (append (multi-move (cons (car friendlies) team) (car enemies)) moves))
                 (set! enemy-cells (filter (lambda (x) (not (= x (car enemies)))) enemy-cells))
                 (set! team ())
                 (set! team-str 0)
                 (plan-helper (cdr friendlies) enemy-cells))
                (else
                  (plan-helper friendlies (cdr enemies)))))


        (plan-helper my-cells enemy-cells)))

    ;;====================;;
    ;; Get-Move Procedure ;;
    ;;===============================================================;;
    ;; This is the procedure called by dots++.scm to get your move.  ;;
    ;; Returns a line-position object.                               ;;
    ;;===============================================================;;

    ;; Main procedure
    (define (get-move player queue board)
      (plan-moves player board queue))


    ;; Return get-move procedure
    get-move

    )) ;; End of player-procedure

