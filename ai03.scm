;;;; CSci 1901 Project - Spring 2012
;;;; PhageWars++ Player AI

;;;======================;;;
;;;  SUBMISSION DETAILS  ;;;
;;;======================;;;

;; List both partners' information below.
;; Leave the second list blank if you worked alone.
(define authors 
  '((
     "Rob Phillips"   ;; Author 1 Name
     "phil0531"   ;; Author 1 X500
     "3706637"   ;; Author 1 ID
     "2"   ;; Author 1 Section
     )
    (
     ""   ;; Author 2 Name
     ""   ;; Author 2 X500
     ""   ;; Author 2 ID
     ""   ;; Author 2 Section
     )))

;; CSELabs Machine Tested On: 
;;


;;;====================;;;
;;;  Player-Procedure  ;;;
;;;================================================;;;
;;; description/strategy:                          ;;;
;;;------------------------------------------------;;;
;;; Alternate attack and defend-all. Always attack ;;;
;;; closest neutral cells first,                   ;;;
;;; then switch to attacking closest enemy cells.  ;;;
;;;================================================;;;
(define player-procedure
  (let () 

    ;;===================;;
    ;; Helper Procedures ;;
    ;;===============================================================;;
    ;; Include procedures used by get-move that are not available in ;;
    ;;  the util.scm or help.scm file.                               ;;
    ;; Note: PhageWars++.scm loads util.scm and help.scm , so you do ;;
    ;;  not need to load it from this file.                          ;;
    ;; You also have access to the constants defined inside of       ;;
    ;;  PhageWars++.scm - Constants are wrapped in asterisks.        ;;
    ;;===============================================================;;

    ;; Returns a random-element from a list.
    (define (random-element lst)
      (list-ref lst (random (length lst))))
    
	;; Makes a random move
    (define (make-random-move player board)
		(let ((my-cells (get-cell-list player board)))
			(list (make-move (car (random-element my-cells)) 
				(car (random-element board))))))

    ;; Returns the closest cell in lst
	(define (get-closest-cell c lst board)
      (define (helper c l closest-yet)
        (cond ((null? l) closest-yet)
              ((iscloser? c closest-yet (caar l) board)
               (helper c (cdr l) closest-yet))
              (else
                (helper c (cdr l) (caar l)))))
      (helper (car c) lst (caar lst)))

    ;; Have every player-owned cell defend
    (define (defend-all player board)
      (define (helper my-cells val)
        (cond ((null? my-cells) val)
              (else
                (helper (cdr my-cells)
                        (cons
                          (make-move (caar my-cells) (caar my-cells))
                          val)))))
      (helper (get-cell-list player board) ()))

    ;; I be scheming and making moves
    (define (land-grab player board)
      (define (helper my-cells neutral-cells enemy-cells val)
        (cond ((null? my-cells) val)
              ((null? enemy-cells) val)
              ((null? neutral-cells)
               (helper
                 (cdr my-cells)
                 neutral-cells
                 enemy-cells
                 (cons
                   (make-move (caar my-cells)
                              (get-closest-cell (car my-cells) enemy-cells board))
                   val)))
              (else
                (helper
                  (cdr my-cells)
                  neutral-cells
                  enemy-cells
                  (cons
                    (make-move (caar my-cells)
                               (get-closest-cell (car my-cells) neutral-cells board))
                    val)))))
      (helper 
        (get-cell-list player board) 
        (get-cell-list *NEUTRAL-SYMBOL* board) 
        (get-cell-list (other-player player) board) 
        ())) 

    ;;====================;;
    ;; Get-Move Procedure ;;
    ;;===============================================================;;
    ;; This is the procedure called by dots++.scm to get your move.  ;;
    ;; Returns a line-position object.                               ;;
    ;;===============================================================;;

	;; Main procedure
    (define (get-move player queue board)
      (if (even? *CURRENT-TURN*)
        (defend-all player board)
        (land-grab player board)))


    ;; Return get-move procedure
    get-move

    )) ;; End of player-procedure
    
