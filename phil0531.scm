;;;; CSci 1901 Project - Spring 2012
;;;; PhageWars++ Player AI

;;;======================;;;
;;;  SUBMISSION DETAILS  ;;;
;;;======================;;;

;; List both partners' information below.
;; Leave the second list blank if you worked alone.
(define authors 
  '((
     "Rob Phillips"   ;; Author 1 Name
     "phil0531"   ;; Author 1 X500
     "3706637"   ;; Author 1 ID
     "2"   ;; Author 1 Section
     )
    (
     "Doua Vang"   ;; Author 2 Name
     "vang1096"   ;; Author 2 X500
     "4220469"   ;; Author 2 ID
     "2"   ;; Author 2 Section
     )))

;; CSELabs Machine Tested On: 
;;


;;;====================;;;
;;;  Player-Procedure  ;;;
;;;====================;;;
(define player-procedure
  (let () 

    ;;===================;;
    ;; Helper Procedures ;;
    ;;===============================================================;;
    ;; Include procedures used by get-move that are not available in ;;
    ;;  the util.scm or help.scm file.                               ;;
    ;; Note: PhageWars++.scm loads util.scm and help.scm , so you do ;;
    ;;  not need to load it from this file.                          ;;
    ;; You also have access to the constants defined inside of       ;;
    ;;  PhageWars++.scm - Constants are wrapped in asterisks.        ;;
    ;;===============================================================;;

    ;; GET-CELL-UNITS
    (define (get-cell-units cell-number board)
      (list-ref (get-cell cell-number board) 4))

    ;; GET-CLOSEST-CELL
    ;; Returns the closest cell in lst
    (define (get-closest-cell c lst board)
      (let loop ((l lst) (closest-yet ()))
        (cond ((null? l) closest-yet)
              ((= c (car l))
               (loop (cdr l) closest-yet))
              ((null? closest-yet)
               (loop (cdr l) (car l)))
              ((iscloser? c closest-yet (car l) board)
               (loop (cdr l) closest-yet))
              (else
                (loop (cdr l) (car l))))))


    ;; UNDER-ATTACK?
    ;; return #t if cell is under attack
    (define (under-attack? cell player queue)
      (define (helper q)
        (cond ((null? (cdr q)) #f)
              ((and
                 (equal? (other-player player)
                         (get-owner-queued (check-front q)))
                 (= cell (get-destination-queued (check-front q))))
               #t)
              (else
                (helper (cons 'queue (cddr q))))))
      (helper queue))

    ;; FILTER-QUEUE
    ;; inputs: predicate and a queue object
    ;; output: queue object with only elements matching the predicate
    (define (filter-queue proc queue)
      (cons 'queue (filter proc (cdr queue))))

    ;; MULTI-FILTER-QUEUE
    ;; inputs: queue object and an arbitrary number of queue predicates
    ;; output: queue object that has been passed through filter-queue for each pred
    (define (multi-filter-queue queue . preds)
      (let loop ((p preds) (q queue))
        (if (null? p) q
            (loop (cdr p) (filter-queue (car p) q)))))

    ;;;FILTER-QUEUE PREDICATES
    (define (q-dest? cell)
      (lambda (x) (= cell (get-destination-queued x))))
    (define (q-intime? turns)
      (lambda (x) (<= (get-time-queued x) turns)))
    (define (q-support? player)
      (lambda (x) (eq? player (get-owner-queued x))))
    (define (q-atk? player)
      (lambda (x) (eq? (other-player player) (get-owner-queued x))))

    ;; GET-INCOMING-MOVES
    ;; returns a queue object containing only moves with destination of cell within turns
    (define (get-incoming-moves cell turns queue)
      (multi-filter-queue queue (q-dest? cell) (q-intime? turns)))

    ;; GET-INCOMING-ATKS
    ;; returns a queue object containing only hostile moves with destination of cell
    (define (get-incoming-atks cell turns board queue)
      (multi-filter-queue queue (q-dest? cell) (q-intime? turns) (q-atk? (get-owner cell board))))

    ;; GET-INCOMING-SUPPORT
    ;; returns a queue object containing only friendly moves with destination of cell
    (define (get-incoming-support cell turns board queue)
      (multi-filter-queue queue (q-dest? cell) (q-intime? turns) (q-support? (get-owner cell board))))

    ;; WILL-SURVIVE?
    ;; returns #t if the cell will not change ownership next turn
    (define (will-survive? cell turns board queue)
      (let ((unit-count (get-cell-units cell board))
            (incoming-atk-str
              (apply + (map unit-count-queued (cdr (get-incoming-atks cell turns board queue)))))
            (incoming-sup-str
              (apply + (map unit-count-queued (cdr (get-incoming-support cell turns board queue))))))
        (>= (+ unit-count incoming-sup-str) incoming-atk-str)))

    ;; GET-SURVIVORS
    ;; return a list of cell numbers that will not be captured by currently incoming atks
    (define (get-survivors turns player board queue)
      (let loop ((cells (get-cell-number-list player board))
                 (survivors ()))
        (cond ((null? cells) survivors)
              ((will-survive? (car cells) turns board queue)
               (loop (cdr cells) (cons (car cells) survivors)))
              (else
                (loop (cdr cells) survivors)))))

    ;; CAPTURE?
    ;; return #t if c1 can capture c2
    (define (capture? c1 c2 board #!optional bonus)
      (let ((time (ceiling (/ (distance c1 c2 board) *MOVE-SPEED*)))
            (attack-strength (get-attack-count c1 board))
            (bonus (if (number? bonus) bonus 0))
            (c2-current-str (get-cell-units c2 board))
            (enrage (get-enrage-value *CURRENT-TURN*)))
        (>= (+ (* attack-strength enrage) bonus) c2-current-str)))

    ;; MULTI-MOVE
    ;; inputs: list of cell numbers and a destination number
    ;; output: list of moves of for each origin number to destination
    (define (multi-move origin-nums destination)
      (if (null? origin-nums) ()
        (cons (make-move (car origin-nums) destination)
              (multi-move (cdr origin-nums) destination))))

    ;; MAKE-PLAN
    ;; I be scheming and making moves
    ;; inputs: player symbol, board, and the current queue
    ;; output: a list of move objects
    (define (make-plan player board q)
      (let ((my-cells (get-cell-number-list player board))
            (enemy-cells (get-survivors 1 (other-player player) board q))
            (neutral-cells (get-survivors 1 *NEUTRAL-SYMBOL* board q))
            (rage (get-enrage-value *CURRENT-TURN*))
            (rush-turns (if (> *NUM-CELLS* 7) 6 3))
            (front ())
            (rush ())
            (support ())
            (team ())
            (team-str 0)
            (moves ()))

        ;; TAGS -- associate each of my-cells with a role
        (let tags ((m my-cells))
          (if (null? m) 'done
            (let ((c (get-closest-cell (car m) (append my-cells enemy-cells neutral-cells) board)))
              (cond ((< *CURRENT-TURN* rush-turns)
                     (set! rush (cons (car m) rush))
                     (tags (cdr m)))
                    ((eq? (other-player player) (get-owner c board))
                     (set! front (cons (car m) front))
                     (tags (cdr m)))
                    ((under-attack? (car m) player q)
                     (set! front (cons (car m) front))
                     (tags (cdr m)))
                    ((> (length support) (length front))
                     (set! front (cons (car m) front))
                     (tags (cdr m)))
                    ((eq? player (get-owner c board))
                     (set! support (cons (car m) support))
                     (tags (cdr m)))
                    (else
                      (set! rush (cons (car m) rush))
                      (tags (cdr m)))))))

        ;; DO-SUPPORT -- make moves for the cells in the 'support' role
        (let do-support ((m support))
          (cond ((null? m) 'done)
                ((null? front)
                 (set! rush (cons (car m) rush))
                 (do-support (cdr m)))
                (else
                  (let ((c (get-closest-cell (car m) front board)))
                    (set! moves (cons (make-move (car m) c) moves))
                    (do-support (cdr m))))))

        ;; DO-RUSH -- make-moves for the cells in the 'rush' role
        (let do-rush ((m rush))
          (cond ((null? m) 'done)
                ((null? neutral-cells)
                 (set! front (cons (car m) front))
                 (do-rush (cdr m)))
                (else
                  (let ((c (get-closest-cell (car m) neutral-cells board)))
                    (set! moves (cons (make-move (car m) c) moves))
                    (set! neutral-cells
                      (filter (lambda (x) (not (= x c))) neutral-cells))
                    (do-rush (cdr m))))))
        
        ;; DO-FRONT -- make moves for the cells in the 'front' role
        (let do-front ((m front))
          (cond ((null? m) 'done)
                ((null? enemy-cells)
                 (set! moves (cons (make-move (car m) (car m)) moves))
                 (do-front (cdr m)))
                (else
                  (let ((c (get-closest-cell (car m) (append enemy-cells neutral-cells) board)))
                    (cond ((capture? (car m) c board)
                           (set! moves (cons (make-move (car m) c) moves))
                           (set! enemy-cells
                             (filter (lambda (x) (not (= x c))) enemy-cells))
                           (do-front (cdr m)))
                          ((capture? (car m) c board team-str)
                           (set! moves (cons (make-move (car m) c) moves))
                           (set! moves (append (multi-move team c) moves))
                           (set! enemy-cells
                             (filter (lambda (x) (not (= x c))) enemy-cells))
                           (set! team ())
                           (set! team-str 0)
                           (do-front (cdr m)))
                          (else
                            (set! team (cons (car m) team))
                            (set! team-str (+ (get-attack-count (car m) board)))
                            (do-front (cdr m))))))))

        ;; DO-TEAM -- make sure that every cell has a move...
        (let do-team ((m team))
          (cond ((null? m) 'done)
                (else
                  (set! moves (cons (make-move (car m) (car m)) moves))
                  (do-team (cdr m)))))

        ;; return my list of moves:
        moves))

    ;;====================;;
    ;; Get-Move Procedure ;;
    ;;===============================================================;;
    ;; This is the procedure called by dots++.scm to get your move.  ;;
    ;; Returns a line-position object.                               ;;
    ;;===============================================================;;

    ;; Main procedure
    (define (get-move player queue board)
      (make-plan player board queue))


    ;; Return get-move procedure
    get-move

    )) ;; End of player-procedure

