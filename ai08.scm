;;;; CSci 1901 Project - Spring 2012
;;;; PhageWars++ Player AI

;;;======================;;;
;;;  SUBMISSION DETAILS  ;;;
;;;======================;;;

;; List both partners' information below.
;; Leave the second list blank if you worked alone.
(define authors 
  '((
     "Rob Phillips"   ;; Author 1 Name
     "phil0531"   ;; Author 1 X500
     "3706637"   ;; Author 1 ID
     "2"   ;; Author 1 Section
     )
    (
     ""   ;; Author 2 Name
     ""   ;; Author 2 X500
     ""   ;; Author 2 ID
     ""   ;; Author 2 Section
     )))

;; CSELabs Machine Tested On: 
;;


;;;====================;;;
;;;  Player-Procedure  ;;;
;;;====================;;;
(define player-procedure
  (let () 

    ;;===================;;
    ;; Helper Procedures ;;
    ;;===============================================================;;
    ;; Include procedures used by get-move that are not available in ;;
    ;;  the util.scm or help.scm file.                               ;;
    ;; Note: PhageWars++.scm loads util.scm and help.scm , so you do ;;
    ;;  not need to load it from this file.                          ;;
    ;; You also have access to the constants defined inside of       ;;
    ;;  PhageWars++.scm - Constants are wrapped in asterisks.        ;;
    ;;===============================================================;;

    ;; CELLS->CELL-NUMS
    ;; inputs: a list of cells
    ;; output: a list of cell numbers
    (define (cells->cell-nums cells)
      (if (null? cells) ()
        (cons (caar cells) (cells->cell-nums (cdr cells)))))

    ;; GET-CLOSEST-CELL
    ;; Returns the closest cell in lst or null if lst is empty
    (define (get-closest-cell cell-num lst board)
      (define (helper lst closest-yet)
        (cond ((null? lst) closest-yet)
              ((null? closest-yet) (helper lst (car lst)))
              ((iscloser? cell-num closest-yet (car lst) board)
               (helper (cdr lst) closest-yet))
              (else
                (helper (cdr lst) (car lst)))))
      (helper (filter (lambda (x) (not (= x cell-num))) lst) ()))


    (define (plan-moves player board q)
      (let ((my-cells (cells->cell-nums (get-cell-list player board)))
            (enemy-cells (cells->cell-nums (get-cell-list (other-player player) board)))
            (neutral-cells (cells->cell-nums (get-cell-list *NEUTRAL-SYMBOL* board)))
            (front-line ())
            (rush ())
            (support ())
            (team ())
            (team-str 0)
            (enrage (get-enrage-value *CURRENT-TURN*))
            (moves ()))

        ;; RESET-TEAM
        (define (reset-team)
          (set! team ()) (set! team-str 0))

        ;; PREP
        (define (prep my-cells all-cells)
          (if (null? my-cells) 
            'done
            (let ((c (get-closest-cell (car my-cells) all-cells board)))
              (display+ "car of my-cells: " (car my-cells))(newline)
              (display+ "closest cell: " c)(newline)
              (display+ "Owner of closest cell: " (get-owner c board)) (newline)
              (cond ((equal? (other-player player) (get-owner c board))
                     (set! front-line (cons (car my-cells) front-line))
                     (prep (cdr my-cells) all-cells))
                    ((equal? *NEUTRAL-SYMBOL* (get-owner c board))
                     (set! rush (cons (car my-cells) rush))
                     (prep (cdr my-cells) all-cells))
                    (else
                      (set! support (cons (car my-cells) support))
                      (prep (cdr my-cells) all-cells))))))

        ;; MAKE-CLOSEST-MOVES
        (define (make-closest-moves my-lst dest-lst board)
          (cond ((null? my-lst) 'done)
                ((null? dest-lst)
                 (set! rush (append my-lst rush)) 'done)
                (else
                  (set! moves (cons (make-move (car my-lst) (get-closest-cell (car my-lst) dest-lst board)) moves))
                  (make-closest-moves (cdr my-lst) dest-lst board))))

        ;; DO-SUPPORT
        (define (do-support)
          (make-closest-moves support front-line board))

        ;; DO-FRONT-LINE
        (define (do-front-line)
          (make-closest-moves front-line enemy-cells board))

        ;; DO-RUSH
        (define (do-rush)
          (make-closest-moves rush (append neutral-cells enemy-cells) board))


        (display "Before prep:")(newline)
        (display+ "support: " support)(newline)
        (display+ "rush: " rush)(newline)
        (display+ "front-line: " front-line)(newline)(newline)

        (prep my-cells (append my-cells neutral-cells enemy-cells))

        (display "After prep:")(newline)
        (display+ "support: " support)(newline)
        (display+ "rush: " rush)(newline)
        (display+ "front-line: " front-line)(newline)(newline)

        (do-support)
        (do-rush)
        (do-front-line)

        (display+ "moves: " moves)(newline)

        moves))
	


    ;;====================;;
    ;; Get-Move Procedure ;;
    ;;===============================================================;;
    ;; This is the procedure called by dots++.scm to get your move.  ;;
    ;; Returns a line-position object.
    ;;===============================================================;;

	;; Main procedure
    (define (get-move player queue board)
      (plan-moves player board queue))


    ;; Return get-move procedure
    get-move

    )) ;; End of player-procedure
    
