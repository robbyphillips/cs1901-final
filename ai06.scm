;;;; CSci 1901 Project - Spring 2012
;;;; PhageWars++ Player AI

;;;======================;;;
;;;  SUBMISSION DETAILS  ;;;
;;;======================;;;

;; List both partners' information below.
;; Leave the second list blank if you worked alone.
(define authors 
  '((
     "Rob Phillips"   ;; Author 1 Name
     "phil0531"   ;; Author 1 X500
     "3706637"   ;; Author 1 ID
     "2"   ;; Author 1 Section
     )
    (
     ""   ;; Author 2 Name
     ""   ;; Author 2 X500
     ""   ;; Author 2 ID
     ""   ;; Author 2 Section
     )))

;; CSELabs Machine Tested On: 
;;


;;;====================;;;
;;;  Player-Procedure  ;;;
;;;====================;;;
(define player-procedure
  (let () 

    ;;===================;;
    ;; Helper Procedures ;;
    ;;===============================================================;;
    ;; Include procedures used by get-move that are not available in ;;
    ;;  the util.scm or help.scm file.                               ;;
    ;; Note: PhageWars++.scm loads util.scm and help.scm , so you do ;;
    ;;  not need to load it from this file.                          ;;
    ;; You also have access to the constants defined inside of       ;;
    ;;  PhageWars++.scm - Constants are wrapped in asterisks.        ;;
    ;;===============================================================;;

    ;; REMOVE-FRONT
    ;; input: queue object
    ;; return: queue without the first item
    ;; modified from help.scm to return a value
    (define (remove-front queue)
      (if (null? (cdr queue)) 
        (error "Queue is already empty, cannot remove front")
        (begin
          (set-cdr! queue (cddr queue))
          queue)))

    ;; GET-CLOSEST-CELL
    ;; Returns the closest cell in lst
    (define (get-closest-cell c lst board)
      (define (helper l closest-yet)
        (cond ((null? l) closest-yet)
              ((iscloser? c closest-yet (car l) board)
               (helper (cdr l) closest-yet))
              (else
                (helper (cdr l) (car l)))))
      (helper lst (car lst)))

    ;; UNDER-ATTACK?
    ;; return #t if cell is under attack
    (define (under-attack? cell player queue)
      (define (helper q)
        (cond ((null? (cdr q)) #f)
              ((and
                 (equal? (other-player player)
                         (get-owner-queued (check-front q)))
                 (= cell (get-destination-queued (check-front q))))
               #t)
              (else
                (helper (cons 'queue (cddr q))))))
      (helper queue))

    (define (filter-queue proc queue)
      (cons 'queue (filter proc (cdr queue))))

    ;; GET-INCOMING-MOVES
    ;; returns a queue object containing only moves with a destination of cell within turns
    (define (get-incoming-moves cell turns queue)
      (filter-queue
        (lambda (x) 
          (and (= cell (get-destination-queued x))
               (>= turns (get-time-queued x))))
        queue))

    ;; GET-INCOMING-ATKS
    ;; returns a queue object containing only hostile moves with destination of cell
    (define (get-incoming-atks cell turns board queue)
      (filter-queue
        (lambda (x)
          (and (= cell (get-destination-queued x))
               (>= turns (get-time-queued x))
               (eq? (other-player (get-owner cell board)) (get-owner-queued x))))
        queue))

    ;; GET-INCOMING-SUPPORT
    ;; returns a queue object containing only friendly moves with destination of cell
    (define (get-incoming-support cell turns board queue)
      (filter-queue
        (lambda (x)
          (and (= cell (get-destination-queued x))
               (>= turns (get-time-queued x))
               (eq? (get-owner cell board) (get-owner-queued x))))
        queue))

    ;; WILL-SURVIVE?
    ;; returns #t if the cell will not change ownership next turn
    (define (will-survive? cell player board queue)
      (let ((income (get-cell-income cell board))
            (unit-count (get-cell-units cell board))
            (q (get-incoming-moves cell 2 queue)))
        (define (helper)
          (cond ((null? (cdr q)) (>= unit-count 0))
                ((equal? player (get-owner-queued (check-front q)))
                 (set! unit-count (+ unit-count (unit-count-queued (check-front q))))
                 (remove-front q)
                 (helper))
                (else
                  (set! unit-count (- unit-count (unit-count-queued (check-front q))))
                  (remove-front q)
                  (helper))))
        (helper)))

    ;; GET-NEXT-LOSSES
    ;; return a list of cell numbers that will be captured from player next turn
    (define (get-next-losses player board queue)
      (define (helper cells losses)
        (cond ((null? cells) losses) 
              ((will-survive? (caar cells) player board queue)
               (helper (cdr cells) losses))
              (else
                (helper (cdr cells) (cons (caar cells) losses)))))
      (helper (get-cell-list player board) ()))

    ;; GET-SURVIVORS
    ;; return a list of cell numbers that will not be captured by currently incoming atks
    (define (get-survivors player board queue)
      (define (helper cells survivors)
        (cond ((null? cells) survivors)
              ((will-survive? (caar cells) player board queue)
               (helper (cdr cells) (cons (caar cells) survivors)))
              (else
                (helper (cdr cells) survivors))))
      (helper (get-cell-list player board) ()))


    ;; GET-TARGETED-CELLS
    ;; return a list of cells that are under attack
    (define (get-targeted-cells player board)
      (define (helper l val)
        (cond ((null? l) val)
              ((under-attack? (caar l))
               (helper (cdr l) (cons (car l) val)))
              (else
                (helper (cdr l) val))))
      (helper (get-cell-list player) ()))

    ;; GET-CELL-INCOME
    ;; inputs: cell number and board
    ;; return: cell income per turn, integer
    (define (get-cell-income cell-number board)
      (list-ref (get-cell cell-number board) 3))

    ;; GET-CELL-UNITS
    (define (get-cell-units cell-number board)
      (list-ref (get-cell cell-number board) 4))

    ;; CAPTURE?
    ;; return #t if c1 can capture c2
    (define (capture? c1 c2 board)
      (let ((time (ceiling (/ (distance c1 c2 board) *MOVE-SPEED*)))
            (attack-strength (get-attack-count c1 board))
            (c2-current-str (get-cell-units c2 board))
            (c2-growth (get-cell-income c2 board))
            (enrage (get-enrage-value *CURRENT-TURN*)))
        ;(>= attack-strength (+ c2-current-str (* c2-growth time)))))
        (>= (* attack-strength enrage) c2-current-str)))

    (define (cell-nums ls)
      (if (null? ls) ()
        (cons (caar ls) (cell-nums (cdr ls)))))

    ;; I be scheming and making moves
    (define (plan-moves player board q)
      (define (plan-helper my-cells enemy-cells val)
        (cond ((null? my-cells) val)
              ((null? enemy-cells)
               (plan-helper
                 (get-cell-list player board)
                 (append
                   (cell-nums (get-cell-list *NEUTRAL-SYMBOL* board))
                   (cell-nums (get-cell-list (other-player player) board)))
                 val))
              (else
                (let ((current (caar my-cells))
                      (closest-enemy (get-closest-cell (caar my-cells) enemy-cells board)))
                  (cond ((capture? current closest-enemy board)
                         (plan-helper
                           (cdr my-cells)
                           enemy-cells
                           (cons (make-move current closest-enemy) val)))
                        (else
                          (plan-helper
                            (cdr my-cells)
                            enemy-cells
                            (cons (make-move current current) val))))))))
      (plan-helper 
        (get-cell-list player board)
        (append
          ;(cell-nums (get-cell-list *NEUTRAL-SYMBOL* board))
          (get-survivors *NEUTRAL-SYMBOL* board q)
          (get-survivors (other-player player) board q))
        ())) 

    ;;====================;;
    ;; Get-Move Procedure ;;
    ;;===============================================================;;
    ;; This is the procedure called by dots++.scm to get your move.  ;;
    ;; Returns a line-position object.                               ;;
    ;;===============================================================;;

    ;; Main procedure
    (define (get-move player queue board)
      (plan-moves player board queue))


    ;; Return get-move procedure
    get-move

    )) ;; End of player-procedure

