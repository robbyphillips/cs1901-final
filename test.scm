(define a '(1 2 3 4 5 6 7 12 18 36))
(define b '((a b c) (a b c) (a b c) 3 4 5 2 s (a b c) a (a b c)))

(define (multi-filter lst . preds)
  (let loop ((p preds) (l lst))
    (if (null? p)
      (begin
        (display "flitered l: ")(display l)(newline)
        l)
      (begin
        (display "l so far: ")(display l)(newline)
        (loop (cdr p) (filter (car p) l))))))

(define (div3? x)
  (= 0 (modulo x 3)))
        
(define (div-by? x)
  (lambda (y)
    (= 0 (modulo y x))))

(define map-syms '(map1 map2 map3 map4 map5 map6 map7))

(define (run-all-tests p1 p2 rounds maps)
  (let loop ((m maps) (results ()))
    (if (null? m)
      (cons (string-append p1 " vs. " p2) results)
      (begin
        (newline)(newline)
        (display (car m))
        (newline)
        (loop (cdr m) (cons (run-tests p1 p2 rounds (car m)) results))))))

(define ais '("random" "ai00" "ai01" "ai02" "ai03" "ai04" "ai05" "ai06" "ai07" "ai08" "ai09"))
(define ais2 '("random" "ai00" "ai09" "ai06" "jaya0089"))


(define (multi-run-tests p1 opponents rounds maps)
  (let loop ((o opponents) (results ()))
    (if (null? o)
      results
      (begin
        (newline)(newline)
        (display (string-append p1 " vs. " (car o)))
        (newline)
        (loop (cdr o) (cons (run-all-tests p1 (car o) rounds maps) results))))))

;;; runs tests in a round-robin fashion
;;; returns output in a list form that is readable by display-ai-report
(define (run-rr-tests player-list rounds maps)
    (let loop ((p1-list player-list)
               (p2-list player-list)
               (results ()))
        (cond ((null? p1-list) results)
              ((null? p2-list)
               (loop (cdr p1-list) player-list results))
              ((string=? (car p1-list) (car p2-list))
               (loop p1-list (cdr p2-list) results))
              (else
                (newline)(newline)
                (display (string-append (car p1-list) " vs. " (car p2-list)))
                (newline)
                (loop p1-list (cdr p2-list) (cons (run-all-tests (car p1-list) (car p2-list) rounds maps) results))))))

;; pretty prints test information produced by multi-run-tests, run-rr-tests
(define (display-ai-report test-output)
 (newline)(newline)
 (cond ((null? test-output)
        (newline) 'done)
       ((string? (caar test-output))
        (display (caar test-output))(newline)
        (let all-tests-loop ((all-tests (cdar test-output)))
          (cond ((null? all-tests)
                 (display-ai-report (cdr test-output)))
                (else
                  (display "Map: ")(display (caar all-tests))(newline)
                  (let each-map ((results (cdar all-tests)))
                    (cond ((null? results)
                           (all-tests-loop (cdr all-tests)))
                          (else
                            (display (string-append 
                                       (symbol->string (caar results)) ": "))
                            (display (cadar results))(newline)
                            (each-map (cdr results)))))))))
       (else "SOMETHING BROKE")))

(define (flatten lst)
  (cond ((null? lst) ())
        ((pair? (car lst)) (append (flatten (car lst)) (flatten (cdr lst))))
        (else
          (cons (car lst) (flatten (cdr lst))))))

(define (whitespace num)
  (let loop ((c 0) (s ""))
    (if (> c num) s
      (loop (+ c 1) (string-append " " s)))))


#| ;; At this point this is a waste of time, but it would've been nice to have earlier..
(define (condense-report test-output)
  (newline)(newline)
  (let ((cols (map flatten test-output)) (num-cols (length test-output)) (col-width 20) (rows 43))
    (let top-loop ((col-count 0) (row-count 0) (rowstring ""))
      (cond 
        ((null? test-output) (newline) 'done)
        ((= col-count num-cols)
         (display rowstring)(newline)
         (top-loop 0 (+ row-count 1) ""))
        ((> row-count rows) (newline) 'done)
        (else
          (let* ((get-col (list-ref cols row-count))
                 (colstring (cond ((null? (car get-col) "NULL")
                                   ((symbol? (car get-col)) (symbol->string (car get-col)))
                                   ((number? (car get-col)) (number->string (car get-col)))
                                   (else (car get-col)))))
                 (len-colstring (string-length colstring)))
            (cond ((or (string=? colstring "p1") (string=? colstring "p2") (string=? colstring "tie"))
                   (top-loop col-count row-count (string-append rowstring colstring ": " (number->string (list-ref get-col
                                                                                                                   |#
